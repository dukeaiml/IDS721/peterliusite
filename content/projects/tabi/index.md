+++
title = "HiResSimCLR"
description = "A PyTorch Lightning implementation of SimCLR for custom backbones & high-resolution image classification."
weight = 1

[extra]
local_image = "projects/tabi/simclr.png"
canonical_url = "https://osc.garden/projects/tabi/"
social_media_card = "social_cards/projects_tabi.jpg"
+++

[**HiResSimCLR**](https://github.com/pl909/hi-res-simclr) is a PyTorch Lightning implementation of SimCLR Contrastive Learning for custom backbones & high-resolution image classification. This implementation was applied to a ~60,00 image fundus image dataset for my Digital Diagnostic's internship - code is released open sourced w/ permission from Digital Diagnostics.

{{ full_width_image(src="projects/tabi/simclr.png", alt="tabi light and dark mode") }}

#### [View on GitHub](https://github.com/pl909/hi-res-simclr) {.centered-text}
## Features

- **[PyTorch Lightning](https://www.conventionalcommits.org)**: The code is primarily split into three parts. The first is the training of a DenseNet121 backbone. It llearns to extracts key features of images by mapping the same image pair close in a vector space and different image pairs far away (hense Contrastive Learning). The second part is the 

- **SimCLR Contrastive Learning**: Implemented [this Unsupervised Contrastive Learning paper](https://arxiv.org/abs/2002.05709) to learn features of eye images. 

- **Dimensions**: Custom model heads are added to the DenseNet121 architecture to support 448x448 image and 896x896 images. 

- **Data Augmentation**: Image pairs are flipped, rotated, color changed, and cropped to add to training data and improve model effectiveness on image variations. 
