+++
title = "Verilog Code Generation"
description = "A Finetuned Code Llama and CodeGen Model for Verilog Generation"
weight = 4

[extra]
local_image = "projects/spectro/verilog_getting_started.webp"
canonical_url = "https://osc.garden/projects/spectro/"
social_media_card = "social_cards/projects_spectro.jpg"
+++

During my research this year in the Duke CEI Lab, I created a language model to generate Verilog code. More descriptions later (Need to Update).

{{ full_width_image(src="projects/spectro/verilog_getting_started.webp", alt="Spectrogram of Jardin du Sommeil Chant d'Amour Sur La Nuit Grandissante, by Tourette") }}

#### [View on GitHub](https://github.com/pl909/llamalog) {.centered-text}

## Features
- **Versatile Format Support**: Ogg, MP3, FLAC, AAC, ape, WMA, MP4, and WAV.
- **BBCode Output**: Automatically generates BBCode for forums or websites.
- **Batch Processing**: Can handle entire directories or specific files.
- **Extensible**: Options for customisation including local storage and imgur uploading.

## Quick Start

1. Download `spectro` to a directory within your PATH, for example, `~/bin`.
2. Grant execute permissions: `chmod +x spectro`.

For a complete installation guide, [read the full documentation](https://github.com/welpo/spectro#install).

## Usage

**Generate BBCode for an Entire Directory:**

```bash
spectro Path/To/Directory/
```

Output:

```
[hide=Spectrograms][size=3]
[url=https://i.imgur.com/ClzzbP8.png]01. Jardin Du Sommeil.flac[/url]
[/size][/hide]
```
