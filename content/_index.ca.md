+++
path = "/"
title = "Publicacions recents"
sort_by = "date"
template = "section.html"

[extra]
header = {title = "Hola! Soc Peter~", img = "img/main.webp", img_alt = "Óscar Fernández, l'autor de tabi" }
section_path = "blog/_index.ca.md"
max_posts = 4
social_media_card = "social_cards/ca.jpg"
+++

Sorry, this website does not support Catalan currently.