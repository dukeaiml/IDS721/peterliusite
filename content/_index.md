+++
path = "/"
title = "Latest posts"
sort_by = "date"
template = "section.html"

[extra]
header = {title = "Hello! I'm Peter", img = "img/faceshot.png", img_alt = "Óscar Fernández, the theme's author" }
section_path = "blog/_index.md"
max_posts = 4
social_media_card = "social_cards/index.jpg"
+++

I'm an AI Engineer and researcher at Duke University Computational Evolutionary Intelligence Lab. My experiences are in Large Language Models and Computer Vision, and I've recently been interested in AI Agents and Multimodality.


