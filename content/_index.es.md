+++
path = "/"
title = "Publicaciones recientes"
sort_by = "date"
template = "section.html"

[extra]
header = {title = "¡Hola! Soy Peter~", img = "img/faceshot.png", img_alt = "Óscar Fernández, el autor de tabi" }
section_path = "blog/_index.es.md"
max_posts = 4
social_media_card = "social_cards/es.jpg"
+++

Soy un ingeniero y investigador de IA en la Universidad de Duke. Mis experiencias están en Modelos de Lenguaje de Gran Escala y Visión por Computadora, y recientemente he estado interesado en Agentes de IA y Multimodalidad.