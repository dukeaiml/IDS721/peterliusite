# Peter's Zola Portfolio Hosted and Deployed on Gitlab Pages

My fast, lightweight, and modern website meant to be a personal portfolio and blog.

https://dukeaiml.gitlab.io/IDS721/peterliusite/ 

# Table of Contents
1. [Video Demo](#demo)
2. [Description](#description)
3. [Documentation for Creation](#Documenatation for Creation)
4. [Theme's Documentation](#Theme's Documentation)

# Demo

See this: https://youtu.be/ITOv5TLoANQ to view video demo. It showcases (no voice because I recorded this in a relatively noisy area) the website functionalities as hosted deployed with a URL through Gitlab and Gitlab's AutoDevOps ( GitLab workflow to build and deploy site on push).


# Description

My Continously Delivered Personal Portfolio features a personal website built with Zola and hosted on GitLab Pages. This website includes a main about section with the text "~Peter" (my name), a blog menu button, a tag exploration feature, and a project page that displays images and descriptions of various projects. The site benefits from an automated CI/CD pipeline via GitLab, which facilitates immediate updates and deployments with each code commit. This setup displays a clean, user-friendly online portfolio with efficient content management and delivery.


# Documentation for Creation

Setup Zola site locally using https://www.getzola.org/documentation/themes/installing-and-using-themes/.

Most of the time this will entail:

$ cd themes
$ git clone <theme repository URL>.

Make sure you are a Maintainer on DukeAIML so you can push to repos. 

Create a repo on DukeAIML or DukeAIML/IDS721Cloud.

cd into your Zola folder. 

Change the contents of the website.

### The contents of the main (about) page can be changed through /content/_index.md.

For example, my content in the file is:

"I'm an AI Engineer and researcher at Duke University Computational Evolutionary Intelligence Lab. My experiences are in Large Language Models and Computer Vision, and I've recently been interested in AI Agents and Multimodality."

### The contents of the blogs can be changed through /blog/{blog_name}/index.md
### The contents of the projects can be changed through /projects/{project_name}/index.md

Push the contents into the repo (gitlab has instructions on this). Set Auto DevOps on in Gitlab.

Follow the instructions here: https://www.getzola.org/documentation/deployment/gitlab-pages/ to deploy it. 

Create a gitlab-ci.yml in the repo with the website’s provided text.

Go to Deploy on the left sidebar -> Pages to find your URL. Hope this helps.


# Theme's Documenatation
See the theme's documentation [here](https://welpo.github.io/tabi).

